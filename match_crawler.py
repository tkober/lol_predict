import riot_api_helper as riot
import sys
import argparse
import pickle
import os


def save_object(dir, name, object):
    if not os.path.exists(dir):
        os.makedirs(dir)

    with open(dir + '/' + name + '.pkl', 'wb') as f:
        pickle.dump(object, f, pickle.HIGHEST_PROTOCOL)


def load_objects(path):
    objects = []
    with (open(path, "rb")) as openfile:
        while True:
            try:
                objects.append(pickle.load(openfile))
            except EOFError:
                break
    return objects


parser = argparse.ArgumentParser(description='League of Legend match crawler.')
parser.add_argument('-s', '--summoner', help='Summoner name to start crawling from', required=True, type=str)
parser.add_argument('-o', '--output', help='Output directory', required=True, type=str)
parser.add_argument('-g', '--goal', help='The number of matches that shall be crawled', required=True, type=int)
parser.add_argument('-k', '--api_key', help='A valid API key for the Riot Games API', required=True, type=str)
parser.add_argument('-r', '--region', help='Region that shall be used', type=str, default='euw1')
parser.add_argument('-d', '--debug', help='Prints debug information such as requests to Riot Games API', action='store_true', default=False)
parser.add_argument('-a', '--autosave', help='Define after how many matches the data shall be autosaved', type=int, default=100)
parser.add_argument('-l', '--load', help="Continue a previous crawling by loading its reaults", type=str)
parser.add_argument('-q', '--queue', help="The queue id that should be used for filtering in match history", type=int)
args = parser.parse_args()

print('\nSTART CRAWLING MATCHES')
print('Summoner:\t' + args.summoner)
print('Region  :\t' + args.region)
print('Queue   :\t' + str(args.queue))
print('Output  :\t' + args.output)
print('Goal    :\t' + str(args.goal))
print('API key :\t' + args.api_key)
print('Debug   :\t' + str(args.debug))
print('Autosave:\t' + str(args.autosave))

# Crawling context
goal_offset = 0                         # In case of loading a previous crawling result
summoners = {}                          # Already visited summoners
candidates = []                         # Summoners to crawl
matches = {}                            # Matches stored as map to efficiently check for duplicates
n_added_matches = 0                     # How many match have been added since the last autosave

# Load previous result if specified
if args.load != None:
    print('\n[INFO]\tloading previous crawling result from "' + args.load + '"')
    matches = load_objects(args.load + '/matches.pkl')[0]
    print('[INFO]\tloaded', len(matches), 'games')
    summoners = load_objects(args.load + '/summoners.pkl')[0]
    print('[INFO]\tloaded', len(summoners), 'summoners')
    goal_offset = len(matches)

# Retrieve account id for first summoner name
print('\nRETRIEVING START SUMMONER INFORMATION')
start_id, start_name = riot.get_account_information_by_summoner_name(args.summoner, args.api_key, region=args.region, debug=args.debug)
print('Id  :\t' + str(start_id))
print('Name:\t' + start_name)
candidates = [(start_id, start_name)]   # Summoners to crawl


# Crawl until goal is reached
goal = args.goal + goal_offset
while len(matches) < goal:

    # Stop if there are no more candidates
    if len(candidates) == 0:
        print('\n[ERROR]\tThere are no more known summoners to crawl')
        break

    # Get next summoner
    account_id, summoner_name = candidates.pop(0)
    # Check if already crawled
    if account_id in summoners:
        if args.debug:
            print('\n[DEBUG]\tskipping "' + summoner_name +  '" (' + str(account_id) + '), already crawled')
        continue

    # Get recent matches
    print('\n[INFO]\tcrawling summoner "' + summoner_name +  '" (' + str(account_id) + ')')
    games = riot.get_match_history(account_id, args.queue, args.api_key, region=args.region, debug=args.debug)
    print('[INFO]\tfound', len(games), 'matches')

    # Mark summoner as crawled
    summoners[account_id] = summoner_name

    # For each match
    for game in games:

        # Add to corpus if it has not been crawled yet
        game_id = game['gameId']
        if game_id not in matches:
            print('[INFO]\tcrawling ' + str(game_id))
            # Get match details
            details = riot.get_match_details(game_id, args.api_key, region=args.region, debug=args.debug)

            # Check if crawling succeeded
            if details is not None:
                # Add to matches
                matches[game_id] = details
                n_added_matches += 1

                # Get participants
                participants = riot.participant_account_ids_from_match(details)
                candidates.extend(participants)

        else:
            print('[INFO]\tmatch ' + str(game_id) + ', already crawled, skipping')

    # Autosave if necessary
    if n_added_matches >= args.autosave:
        print('\n[INFO]\tautosaving')

        save_object(args.output, 'summoners', summoners)
        print('[INFO]\tsaved summoners')

        save_object(args.output, 'matches', matches)
        print('[INFO]\tsaved matches')

        n_added_matches = 0

    print('\n[INFO]\t', len(matches) - goal_offset, 'of', args.goal, 'matches' )


print('\n[INFO]\tCrawled', len(matches), 'matches from', len(summoners), 'summoners')

print('\n[INFO]\tSaving final corpus')

save_object(args.output, 'summoners', summoners)
print('[INFO]\tsaved summoners')

save_object(args.output, 'matches', matches)
print('[INFO]\tsaved matches')
