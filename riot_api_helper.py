import requests
import time

def execute_riot_api_request(url, api_key, params=[], debug=False):
    req = url + '?api_key=' + api_key
    for param in params:
        req += '&' + param 

    status_code = -1
    retry_count = 0
    while status_code != 200:
        if debug:
            print('[DEBUG]\tapi call ', req)
        resp = requests.get(req)
        retry_count += 1
        status_code = resp.status_code

        # Limit exceeded
        if status_code == 429:
            if 'Retry-After' in resp.headers:
                retry_after = int(resp.headers['Retry-After']) + 1
                print('[WARN]\tAPI rate limit exceeded, waiting for', retry_after, 'seconds')
                time.sleep(retry_after)
                print('[INFO]\tretry')

        # Different Error
        else:
            if status_code != 200:
                print('[WARN]\trequest failed:', resp.text)

        if retry_count > 5:
            print('[ERROR]\texceeded retry count')
            return None

    return resp.json()


def get_account_information_by_summoner_name(summoner_name, api_key, region='euw1', debug=False):
    url = 'https://' + region + '.api.riotgames.com/lol/summoner/v3/summoners/by-name/' + summoner_name
    json = execute_riot_api_request(url, api_key, debug=debug)
    return (json['accountId'], json['name'])


def get_match_history(account_id, queue, api_key, region='euw1', debug=False):
    params = []
    if queue is not None:
        params = [ 'queue=' + str(queue) ]

    url = 'https://' + region + '.api.riotgames.com/lol/match/v3/matchlists/by-account/' + str(account_id)
    json = execute_riot_api_request(url, api_key, debug=debug, params=params)
    if json is None:
        return []
    return json['matches']


def get_match_details(match_id, api_key, region='euw1', debug=False):
    url = 'https://' + region + '.api.riotgames.com/lol/match/v3/matches/' + str(match_id)
    json = execute_riot_api_request(url, api_key, debug=debug)
    return json


def participant_account_ids_from_match(match_json):
    if match_json['queueId'] == 0:
        return []
    players = [ (participant['player']['accountId'], participant['player']['summonerName'])
               for participant in match_json['participantIdentities'] ]
    return players
